let boutonAjouter = document.getElementsByTagName("button")[0]; // bouton ajouter
let nouvelElement = document.getElementById("ajouter"); // champ de texte
let aFaire = document.getElementById("toDo"); // liste A Faire
let checkBoxes = document.getElementsByClassName("checkbox"); // les différents To Do sous forme de html collection
let fait = document.getElementById("done"); // liste Fait
let supprimer = document.getElementsByClassName("supprimer"); // les différentes images de croix pour supprimer sous forme de html collection

// Déplacer un élément--------------------------------------------------------------------------------------
deplacerElement = (element) => {
    if (element.checked) {
        fait.appendChild(element.parentNode.parentNode); // déclare que élément et son parent sont maintenant enfants de fait           
    } else if (element.checked === false) {
        aFaire.appendChild(element.parentNode.parentNode); // déclare que élément et son parent sont maintenant enfants de aFaire
    }
};

for (let item of checkBoxes) { // sorte d'équivalent à .forEach() qui ne fonctionne pas pour les html collections
    item.onclick = () => {
        deplacerElement(item);
    };
};

// Supprimer un élément-----------------------------------------------------------------------------------------
supprimerElement = (element) => {
    element.parentNode.remove(); // supprime le parent (et donc élément aussi)
};

for (let item of supprimer) {
    item.onclick = () => {
        supprimerElement(item);
    }
};

// Ajouter un élément-------------------------------------------------------------------------------------------------
ajouterElement = () => {
    if (nouvelElement.value === "") {
        return null;
    }

    let listItem = document.createElement("li");
    let span = document.createElement("span")
    let checkBox = document.createElement("input");
    let text = document.createTextNode(nouvelElement.value + " ");
    let supprimer = document.createElement("img");
    checkBox.type = "checkbox";
    checkBox.className = "checkbox";
    supprimer.className = "supprimer";
    supprimer.src = "./image/supprimer.png";
    supprimer.alt = "bouton supprimer";
    span.appendChild(checkBox); // déclare que checkBox est un enfant de span
    span.appendChild(text); // déclare que text est un enfant de span
    listItem.appendChild(span); // déclare que span est un enfant de listItem
    listItem.appendChild(supprimer); // déclare que supprimer est un enfant de listItem
    aFaire.appendChild(listItem); // déclare que listItem et tous ses enfants sont des enfants de aFaire

    checkBox.onclick = () => {
        deplacerElement(checkBox);
    };

    supprimer.onclick = () => {
        supprimerElement(supprimer);
    }

    nouvelElement.value = "";
    localStorage.setItem("Student", LiStudent.innerHTML);
}

// Ajouter en cliquant sur le bouton------------------------
boutonAjouter.onclick = (event) => {
    ajouterElement();
}

// Ajouter en appuyant sur entrée---------------------------
nouvelElement.onkeyup = (event) => {
    if (event.keyCode === 13) { // la key 13 correspond à la touche entrée
        ajouterElement();
    }
}



let currentName = localStorage.getItem("Student"); // ---------------------------------------------------Va chercher les valeurs stocker dans le local storage avec la key Student
let currentSujet = localStorage.getItem("Sujet"); // ----------------------------------------------------Va chercher les valeurs stocker dans le local storage avec la key Sujet

if (currentName) { // -----------------------------------------------------------------------------------Si currentName a une valeur, alors LiSudent depuis currentName
    LiStudent.innerHTML = currentName;
}

if (currentSujet) { // -----------------------------------------------------------------------------------Si currentSujet a une valeur, alors LiSudent depuis currentSujet 
    LiSujet.innerHTML = currentSujet;
}
